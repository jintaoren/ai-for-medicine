# Week 1 Eye Disease and Cancer Diagnosis

> Detecting DR(diabetic retinopathy) is a time-consuming and manual process that requires a trained clinician to examine these photos. In this study, an algorithm
was developed to determine whether patients had diabetic retinopathy by looking at such photos.


# Data

This study used over 128,000 images of which only 30 percent had diabetic retinopathy. We'll look at this data imbalanced problem,
![](20201007112648.png)