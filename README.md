# AI for medicine

This is a **private** repository to save codes/notes for Coursera course [**AI for Medicine Specialization**](https://www.coursera.org/specializations/ai-for-medicine).


## Part I: AI for Medical Diagnosis
## Part II: AI for Medical Prognosis
## Part III: AI For Medical Treatment


